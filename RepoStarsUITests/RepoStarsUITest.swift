//
//  RepoStarsUITests.swift
//  RepoStarsUITests
//
//  Created by Andre Paganin on 21/08/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import XCTest

class RepoStarsUITests: XCTestCase {

    override func setUpWithError() throws {

        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
    }

    func testExample() throws {
        let app = XCUIApplication()
        app.launch()

    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
