//
//  Repositorie.swift
//  Swift Repo Stars
//
//  Created by Andre Paganin on 21/08/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import Foundation
import UIKit

struct Repositories {
        
    var repositories: [Repositorie]

    struct Repositorie: Codable  {
     
        var name: String?
        var owner: Owner?
        var starCount: Int?
        
        private enum CodingKeys: String, CodingKey {
                   case starCount = "stargazers_count"
                   case owner
                   case name
               }

        
        struct Owner: Codable  {
            var id: Int?
            var login: String?
            var avatarUrl: String?
            
            private enum CodingKeys: String, CodingKey {
                case avatarUrl = "avatar_url"
                case login
                case id
            }
        }
    }
    
}




extension Repositories {
    
    init(from service: GitHubAPIService) {

        repositories = []

        for gitRepo in service.items {
            var repositorie = Repositorie()

            repositorie.name = gitRepo.name
            repositorie.starCount = gitRepo.starCount
            repositorie.owner = gitRepo.owner

            self.repositories.append(repositorie)
        }

    }
    
}


