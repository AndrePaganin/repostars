//
//  ServiceModel.swift
//  Swift Repo Stars
//
//  Created by Andre Paganin on 21/08/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import Foundation

struct GitHubAPIService: Decodable {
    
    var totalCount: Int?
    var items: [Repositories.Repositorie]
    
    private enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case items
    }
    
    struct GitRepositorie: Decodable  {
        
        var name: String?
        var owner: Repositories.Repositorie.Owner
        var starCount: Int?
        
        private enum CodingKeys: String, CodingKey {
            case starCount = "stargazers_count"
            case owner
            case name
        }
        
    }
}
