//
//  SceneDelegate.swift
//  RepoStars
//
//  Created by Andre Paganin on 21/08/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    private var applicationCoordinator: ApplicationCoordinator?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        
        let window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        
        self.window = window
        self.window?.windowScene = windowScene
        let applicationCoordinator = ApplicationCoordinator(window: window)
        self.applicationCoordinator = applicationCoordinator
        
        
   
        applicationCoordinator.start()
        
    }

   


}

