//
//  RepoCoodinator.swift
//  RepoStars
//
//  Created by Andre Paganin on 07/09/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import Foundation
import UIKit

class RepoCoordinator: Coordinator {
    
    private let presenter: UINavigationController
    var repoViewController: ReposTableViewController?
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    func start() {
        let repoViewController = ReposTableViewController(nibName: nil, bundle: nil)
        repoViewController.title = Project.Localizable.Repo.selfTitle.localized
        presenter.pushViewController(repoViewController, animated: true)
        
        self.repoViewController = repoViewController
    }
    
    func stop() {
        presenter.dismiss(animated: true, completion: nil)
    }

    
}
