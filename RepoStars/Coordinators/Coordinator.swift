//
//  Coordinator.swift
//  RepoStars
//
//  Created by Andre Paganin on 07/09/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//


import Foundation
import UIKit

protocol Coordinator: class {
    func start()
    
    func stop()
    
}

extension Coordinator {
    
    func stop() { }
}
