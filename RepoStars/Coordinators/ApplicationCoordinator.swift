//
//  ApplicationCoordinator.swift
//  RepoStars
//
//  Created by Andre Paganin on 07/09/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import Foundation
import UIKit

class ApplicationCoordinator: Coordinator {
    private let window: UIWindow
    private let rootViewController: UINavigationController
    private var homeCoordinator: HomeCoordinator

    
    init(window: UIWindow) {
        
        self.window = window
        rootViewController = UINavigationController()
        
        rootViewController.navigationBar.appUINavigationBarLayout()
        rootViewController.navigationBar.backgroundColor = .white
        
        homeCoordinator = HomeCoordinator(presenter: rootViewController)

    }
    
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        homeCoordinator.start()


    }
}
