//
//  JsonParseUtil.swift
//  Swift Repo Stars
//
//  Created by Andre Paganin on 21/08/20.
//  Copyright © 2020 Andre Paganin. All rights reserved.
//

import Foundation


class JsonParseUtil {
    
    class func fetchRepositoriesData(page: Int, completion: @escaping (Result<GitHubAPIService, Error>) -> ()) {
                
        let pageUrl =  Project.url + page.description

        guard let url = URL(string: pageUrl) else { return }
        
        
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            guard let data = data else { return }
            
            guard let decodedData = try? JSONDecoder().decode(GitHubAPIService.self, from: data) else {
                
                guard let erro = err else { return }
                completion(Result.failure(erro))
                return
            }
                        
            completion(Result.success(decodedData))
        }.resume()
        
    }
    
}
